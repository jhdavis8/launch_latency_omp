#!/bin/bash

if [[ -z "$1" ]]; then
    compiler="clang"
else
    compiler=$1
fi

module purge

if [[ "$compiler" == "cray-classic" ]]; then
    module load esslurm
    module load PrgEnv-cray
    module load cdt/19.11
    module switch cce cce/9.1.3-classic
    module load craype-x86-skylake
    module unload cray-libsci
    module load cudatoolkit craype-accel-nvidia70
    export CUDA_PATH=${CRAY_CUDATOOLKIT_DIR}
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CRAY_CUDATOOLKIT_DIR}/lib64
elif [[ "$compiler" == "cray-llvm" ]]; then
    module load esslurm
    module load PrgEnv-cray
    module load cdt/20.06
    module load craype-x86-skylake
    module load cuda/11.0.2
    module load openmpi/4.0.3
    module unload cray-libsci
elif [[ "$compiler" == "pgi" ]]; then
    module load esslurm
    module load pgi/20.4
    module load cuda
elif [[ "$compiler" == "xl" ]]; then
    module load xl/16.1.1-5
    module load spectrum-mpi/10.3.1.2-20200121
    module load hsi/5.0.2.p5
    module load xalt/1.2.0
    module load lsf-tools/2.0
    module load darshan-runtime/3.1.7
    module load DefApps
    module load cuda/10.1.243
else
    module load esslurm
    module load PrgEnv-llvm/11.0.0-git_20200409
fi

module list

make clean

if [[ -z "$2" ]]; then
    make COMPILER=$compiler build
else
    make COMPILER=$compiler $2
fi
