#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "nvToolsExt.h"
#include <omp.h>

#define N 64

int runtime_init(void)
{
  double x[N];
  double sum, expected_sum, diff, abs_diff;
  struct timeval start_time, stop_time, elapsed_time;
  int i, rtn;

  for (i=0; i<N; ++i) x[i] = (double)i;
  expected_sum = (N/2.) * (N - 1.);
  sum = 0.0;

  gettimeofday(&start_time,NULL);

#ifdef _OPENMP
#pragma omp target teams distribute parallel for map(to:x[0:N]) map(tofrom:sum) reduction(+:sum)
#endif
#ifdef _OPENACC
#pragma acc parallel loop copyin(x[0:N]) copy(sum) reduction(+:sum)
#endif
  for (i=0; i<N; ++i) sum += x[i];

  gettimeofday(&stop_time,NULL);
  timersub(&stop_time, &start_time, &elapsed_time);


  /* Calculate absolute value inline to avoid dependency
     on libm which causes compilation issues on x86
     when using Clang compiler and OpenMP target offload */
  diff = sum - expected_sum;
  abs_diff = diff >= 0.0 ? diff : -diff;
  rtn = abs_diff < 1.0E-6 ? 0 : 1;

  printf("Runtime initialization time: %.6lf seconds (rtn=%d)\n",
     elapsed_time.tv_sec+elapsed_time.tv_usec/1000000.0, rtn);

  return rtn;
}

int empty_kernel(int a[]) {
#ifdef _OPENMP
#pragma omp target teams distribute parallel for
#endif
#ifdef _OPENACC
#pragma acc parallel loop present(a[0:N])
#endif
  for (int i = 0; i < N; i++) {
    a[i] += 1;
  }

  return 0;
}

int main(int argc, char *argv[]) {
  nvtxRangeId_t id1;
  struct timeval start, end;

  runtime_init();

  int a[N] = {0};

#ifdef _OPENACC
#pragma acc enter data copyin(a[0:N])
#endif
#ifdef _OPENMP
#pragma omp target enter data map(to: a[0:N])
#endif

  gettimeofday(&start, NULL);
  id1 = nvtxRangeStartA("launch");

  empty_kernel(a);

  nvtxRangeEnd(id1);
  gettimeofday(&end, NULL);

  printf("Exec time (us): %ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
                                   - (start.tv_sec * 1000000 + start.tv_usec)));

  return 0;
}
