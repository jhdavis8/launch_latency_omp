.PHONY: build run prof nsys clean

ifndef COMPILER
  COMPILER = clang
endif

RUNCMD = srun

ifeq ($(COMPILER),pgi)
  CC = pgcc
  CFLAGS = -fast -ta=tesla:cc70
else ifeq ($(COMPILER),cray-llvm) # Cray 9.x or later
  CC = CC
  CFLAGS = -O3 -fopenmp -fopenmp-targets=nvptx64 -Xopenmp-target -march=sm_70
else ifeq ($(COMPILER),cray-classic) # Cray 9.x or later (classic version)
  CC = CC
  CFLAGS = -h omp -h noacc -g
else ifeq ($(COMPILER),xl) # IBM XL compiler (Summit)
  CC = xlc++
  CFLAGS = -std=c++11 -Ofast -qsmp=omp:noauto -qoffload -qtgtarch=sm_70 -DGPU
  CUDA_PATH = $(CUDAPATH)
  RUNCMD =
else
  CC = clang
  CFLAGS = -O3 -g
  CFLAGS += -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda
endif


latency_measure.exe: latency_measure.c
	$(CC) $(CFLAGS) -ldl -I$(CUDA_PATH)/include/nvtx3 -o $@ $<

build: latency_measure.exe

run: latency_measure.exe
	$(RUNCMD) $<

prof: latency_measure.exe
	$(RUNCMD) nvprof --normalized-time-unit us ./$<
#	srun nvprof -f -o $(COMPILER)_launch_latency_profile.nvvp ./$<

nsys: latency_measure.exe
	srun nsys profile --trace=cuda,nvtx -o $(COMPILER)_launch_latency ./$<

clean:
	rm -f *.exe
